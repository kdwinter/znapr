# Version 1.3.1

* Added `--prefix` option. Defaults to `znapr_`.


# Version 1.3.0

* Added `rollback` command


# Version 1.2.0

* Use commands and not options for command flags (`--list` -> `list`, etc)


# Version 1.1.1

* Refactor all raw Open3.popen3 calls into single callback method


# Version 1.1.0

* Added `autoremove` functionality to snapshot creation
* Automatically detect whether `sudo` (root rights) are required for given operations


# Version 1.0.0

* Initial release supporting `list`, `create`, `delete`
