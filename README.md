# znapr

`znapr` is a basic zfs snapshot manager, functionally similar to the upstream
`zfs-auto-snapshot` utility.

by default, `znapr` will operate on datasets for which `com.sun:auto-snapshot`
is `true`. a specific dataset regardless of the value of this property may be
selected using the `-d` flag.

## actions

List snapshots in selected datasets:

    $ znapr list
    📂 snapshots in storage/root/wiki:
       • storage/root/wiki@2019-01-10         🕛 Thu Jan 10 13:09 2019  💾  152K
       • storage/root/wiki@2019-01-13         🕛 Sun Jan 13 14:16 2019  💾    0B

    📂 snapshots in zroot/arch/HOME/gig:
       • zroot/arch/HOME/gig@2019-01-10       🕛 Thu Jan 10 12:58 2019  💾  536M
       • zroot/arch/HOME/gig@2019-01-12       🕛 Sat Jan 12 13:29 2019  💾  307M
       • zroot/arch/HOME/gig@2019-01-13       🕛 Sun Jan 13 14:19 2019  💾    0B

    📂 snapshots in zroot/arch/ROOT/default:
       • zroot/arch/ROOT/default@2019-01-10   🕛 Thu Jan 10 12:58 2019  💾  260M
       • zroot/arch/ROOT/default@2019-01-12   🕛 Sat Jan 12 13:29 2019  💾 67.7M
       • zroot/arch/ROOT/default@2019-01-13   🕛 Sun Jan 13 14:20 2019  💾    0B

Create new snapshots in selected datasets:

    $ znapr create --format hourly --keep 24
    $ znapr create --format daily --keep 7 --very-quiet --no-confirm
    $ znapr create --format monthly --keep 12 --no-autoremove

    etc...

Delete named snapshots in selected datasets:

    $ znapr delete 2019-01-10
     ℹ removing snapshot 2019-01-10 in storage/root/wiki
     ℹ about to run `sudo zfs destroy storage/root/wiki@2019-01-10`
     ❗confirm? [y/N] n

Roll back to the most recent snapshot in selected datasets:

    $ znapr rollback
     ℹ rolling back to snapshot 2019-01-19 in storage/root/wiki
     ℹ about to run `sudo zfs rollback storage/root/wiki@2019-01-19`
     ❗confirm? [y/N] n

## options

    usage: znapr [action] [options]

    actions:
        list                             list all available snapshots for selected datasets
        create                           create snapshots in configured format for selected datasets
        delete NAME                      delete snapshots named NAME in selected datasets
        rollback                         rollback to the most recent snapshot in selected datasets

    runtime options:
        -d, --dataset=NAME               set dataset to be acted upon. default: all datasets for which `com.sun:auto-snapshot` is true

    additional options:
        -f, --format=FORMAT              set snapshot format (strftime argument) [%F]
        -k, --keep=NUM                   amount of old snapshots to keep [3]
            --[no-]autoremove            automatically prune old snapshots [true]
        -q, --[no-]quiet                 run quietly [false]
        -Q, --very-quiet                 run VERY quietly [false]
        -n, --no-confirm                 don't ask for action confirmation [false]
        -x, --dry-run                    dry run (only *show* commands that would run) [false]
        -v, --version                    print the version
        -h, --help                       prints this message

    znapr Copyright (C) 2019 Kenneth De Winter
    This program comes with ABSOLUTELY NO WARRANTY; see LICENSE for details.
    This is free software, and you are welcome to redistribute it under certain
    conditions; see LICENSE for details.

## wishlist

* add tests
